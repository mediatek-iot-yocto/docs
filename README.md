# Build Radxa NIO 12L IoT Yocto

## download recipe with manifests

```
mkdir ~/iot-yocto; cd ~/iot-yocto
export PROJ_ROOT=`pwd`
repo init -u https://gitlab.com/mediatek/aiot/bsp/manifest.git -b refs/tags/rity-kirkstone-v23.2 -m default.xml --no-repo-verify
repo sync
```

## build configuration

```
TEMPLATECONF=$PWD/src/meta-rity/meta/conf source src/poky/oe-init-build-env
export BUILD_DIR=`pwd`
```

## Setup paths for downloads and sstate-cache folders

```
echo DL_DIR = \"\${TOPDIR}/../downloads\" >> ${BUILD_DIR}/conf/local.conf
echo SSTATE_DIR = \"\${TOPDIR}/../sstate-cache\" >> ${BUILD_DIR}/conf/local.conf
```

## Build rity-demo-image for G1200 EVK board with UFS

Fetch all the packages and build rity-demo-image for UFS version.

```
MACHINE=genio-1200-evk-ufs bitbake rity-demo-image
```

## Build rity-demo-image for Radxa NIO 12L board with UFS

Change meta-mediatek-bsp repository.

```
cd ~/iot-yocto/src/meta-mediatek-bsp/
git remote add gitlab git@gitlab.com:mediatek-iot-yocto/meta-mediatek-bsp.git
git checkout -b rity-kirkstone-v23.2 remotes/gitlab/rity-kirkstone-v23.2
```

Change linux repository.

```
cd ~/iot-yocto
export PROJ_ROOT=`pwd`
TEMPLATECONF=$PWD/src/meta-rity/meta/conf source src/poky/oe-init-build-env
export BUILD_DIR=`pwd`
```

Now we are in `~/iot-yocto/build` directory. Add `build/workspace` to bblayer.

```
devtool modify linux-mtk
```

And we will get `build/workspace/sources/linux-mtk/`. Use new linux repository.

```
cd ~/iot-yocto/build/workspace/sources/linux-mtk/
git remote add gitlab git@gitlab.com:mediatek-iot-yocto/linux.git
git checkout -b mtk-v5.15-dev-rity-kirkstone-v23.2 remotes/gitlab/mtk-v5.15-dev-rity-kirkstone-v23.2
cp arch/arm64/boot/dts/mediatek/genio-1200-radxa-nio-12l.dts arch/arm64/boot/dts/mediatek/genio-1200-evk-ufs.dts
```

Start build rity-demo-image

```
cd ~/iot-yocto
export PROJ_ROOT=`pwd`
TEMPLATECONF=$PWD/src/meta-rity/meta/conf source src/poky/oe-init-build-env
export BUILD_DIR=`pwd`
MACHINE=genio-1200-evk-ufs bitbake rity-demo-image
```

And we will get images under `build/tmp/deploy/images/genio-1200-evk-ufs`.
